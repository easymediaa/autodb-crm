<?php

namespace Crm;

/**
 * <p>Projekt-Autolaoder.</p>
 * <p>Diese Klasse stellt den Autolaoder für Klassen zur Verfügung. Es ist auf
 * ein definiertes Namensschemata zu achten. Effektiv kann man hier machen,
 * was man will.</p>
 * <p>Eine gute Vorgabe wäre nach psr-4 im Ordner /application.</p>
 */
class ClassLoader {

    public static function autoload($strClassName) {
        $arrRequestedClass = explode("\\", $strClassName);

        $arrClassPath = array();
        $arrClassPath[] = self::getApplicationClassPath($arrRequestedClass);

        foreach ($arrClassPath as $strClassPath) {
            if ($strClassPath && file_exists($strClassPath) && is_file($strClassPath)) {
                require_once $strClassPath;
                return true;
            }
        }

        throw new \LogicException("Classfile for '" . $strClassName . "' not found!", E_USER_ERROR);
    }

    private static function getApplicationClassPath($arrRequestedClass) {
        if (reset($arrRequestedClass) !== __NAMESPACE__) {
            return false;
        } else {
            // Unterordner /application
            array_unshift($arrRequestedClass, "application");
            return implode(DIRECTORY_SEPARATOR, $arrRequestedClass) . ".php";
        }
    }

}

error_reporting(E_ALL);
date_default_timezone_set('Europe/Berlin');

require_once 'vendor/autoload.php';

spl_autoload_register(array("Crm\ClassLoader", "autoload"));

if (!session_id()) {
    session_start();
}
