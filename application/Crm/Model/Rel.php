<?php

namespace Crm\Model;

use \workerbeeeu\autodb\Entity\Database\Blueprint;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Rel extends Blueprint {
    
    public function setTable($strTable) {
        parent::setTable($strTable);
    }
    
}