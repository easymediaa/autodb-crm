<?php

namespace Crm\Model\Log;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    public function loadAll() {
        $this->genericLoad('select * from log order by timestamp desc limit 0,50', 'workerbeeeu\autodb\Entity\Log');
    }

}
