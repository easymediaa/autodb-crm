<?php

namespace Crm\Model;

use Crm\Model\Access;
use Crm\Model\Branch;
use workerbeeeu\autodb\Entity\User as AutodbUser;

/**
 * Repräsentation ines Sytemnutzers.
 * Die Klasse dient als Wrapper für {see \workerbeeeu\autodb\Entity\User}, damit der Core
 * für Anapssungen nicht angerührt werden muss.
 *
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 *
 * @property string $anrede
 * @property string $vorname Vorname
 * @property string $nachname Nachname
 */
class User extends AutodbUser {

    private $objAccess = null;

    /**
     * Holt ein Benutzerrechte-Objekt für den aktuellen Benutzer.
     * @param boolean $boolForceReload Erzwingt das Neuladen der Benutzerrechte
     * @return Access Objekt zum Abfragen von Benutzerrechten
     */
    public function getAccess($boolForceReload = false) {
        if ($boolForceReload || is_null($this->objAccess)) {
            $this->objAccess = new Access($this->getDatasource());
            if ($this->access) {
                try {
                    $this->objAccess->load($this->access);
                } catch (Exception $ex) {
                    // @todo muss hier was passieren?
                }
            }
        }
        return $this->objAccess;
    }

    private $objBranch = null;

    public function getBranch($boolForceReload = false) {
        if ($boolForceReload || is_null($this->objBranch)) {
            /* @var $objStatement \workerbeeeu\autodb\PDOStatement */
            $objStatement = $this->getDatasource()->prepare("SELECT branch.id as id FROM rel_branch_user right join branch on branch.id = rel_branch_user.branch_id right join user on user.id = rel_branch_user.user_id WHERE user.id = ?");
            $objStatement->execute(array(
                $this->id));
            $arrIdData = $objStatement->fetch();
            if (isset($arrIdData['id'])) {
                $this->objBranch = new Branch($this->getDatasource());
                $this->objBranch->load($arrIdData['id']);
            }
        }
        return $this->objBranch;
    }

}
