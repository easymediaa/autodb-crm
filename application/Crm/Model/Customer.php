<?php

namespace Crm\Model;

use \workerbeeeu\autodb\Entity\Database\Blueprint;

/**
 * Abbildung von Kunden.
 *
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 *
 * @property int $id Primärschlüssel
 */
class Customer extends Blueprint {

}
