<?php

namespace Crm\Model;

/**
 * Ein Alert beschreibt den Status einer vorherigen Aktion und wird über die
 * gesamte Session hinweg mitgenommen.
 *
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Alert {

    const TYPE_SUCCESS = 1;
    const TYPE_INFO = 2;
    const TYPE_WARNING = 3;
    const TYPE_DANGER = 4;

    private $intType = 1;
    private $strMessagePattern = 1;
    private $arrArgs = array();

    public function __construct($strMessagePattern = '', $intType = 1) {
        $this->intType = $intType;
        $this->strMessagePattern = $strMessagePattern;
    }

    public function isSuccess() {
        return $this->intType == self::TYPE_INFO;
    }

    public function isInfo() {
        return $this->intType == self::TYPE_INFO;
    }

    public function isWarning() {
        return $this->intType == self::TYPE_WARNING;
    }

    public function isDanger() {
        return $this->intType == self::TYPE_DANGER;
    }

    public function setType($intType) {
        $this->intType = $intType;
    }

    public function getType() {
        return $this->intType;
    }

    public function setMessage($strMessage) {
        $this->strMessagePattern = $strMessage;
    }

    public function getMessage() {
        return $this->strMessagePattern;
    }

    public function addArgument($intPos, $strValue) {
        if (is_null($strValue)) {
            unset($this->arrArgs[$intPos]);
        } else {
            $this->arrArgs[$intPos] = $strValue;
        }
    }

    public function output($boolPrint = true) {
        $strBefore = '';
        switch ($this->getType()) {
            case self::TYPE_SUCCESS:
                $strClass = 'success';
                $strBefore = '<span class="glyphicon glyphicon-ok"></span> ';
                break;
            case self::TYPE_INFO:
                $strClass = 'info';
                $strBefore = '<span class="glyphicon glyphicon-star"></span> ';
                break;
            case self::TYPE_WARNING:
                $strClass = 'warning';
                $strBefore = '<span class="glyphicon glyphicon-remove"></span> ';
                break;
            case self::TYPE_DANGER:
                $strClass = 'danger';
                $strBefore = '<span class="glyphicon glyphicon-fire"></span> ';
                break;
        }
        $arrParams = $this->arrArgs;
        array_unshift($arrParams, $this->strMessagePattern);
        $strMessage = call_user_func_array('sprintf', $arrParams);
        $strMarkup = sprintf('<div class="alert alert-%s" role="alert">%s%s</div>', $strClass, $strBefore, $strMessage);
        if ($boolPrint) {
            echo $strMarkup;
        } else {
            return $strMarkup;
        }
    }

}
