<?php

namespace Crm\Model;

use Crm\Model\Alert\Collection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Alerts {

    /**
     * @return Sternauto\Model\Alert\Collection;
     */
    static public function getCollection() {
        if (!isset($_SESSION['Sternauto'])) {
            $_SESSION['Sternauto'] = array();
        }
        if (!isset($_SESSION['Sternauto']['Alerts']) || !is_object($_SESSION['Sternauto']['Alerts'])) {
            $_SESSION['Sternauto']['Alerts'] = new Collection();
        }
        return $_SESSION['Sternauto']['Alerts'];
    }

    static public function attach(Alert$object, $data = null) {
        self::getCollection()->attach($object, $data);
    }

    static public function output() {
        $objCollection = self::getCollection();
        foreach ($objCollection as $objAlert) {
            $objAlert->output();
        }
        $objCollection->removeAll($objCollection);
    }

}
