<?php

namespace Crm\Model\Customer;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    private $arrRevisionCounter = null;

    public function loadAll() {
        $this->genericLoad('select * from customer where 1 order by name desc', __NAMESPACE__);
    }

}
