<?php

namespace Crm\Model\Access;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    public function loadAll() {
        $this->genericLoad('select * from access', 'Crm\Model\Access');
    }

}
