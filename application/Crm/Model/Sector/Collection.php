<?php

namespace Crm\Model\Sector;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    public function loadAll() {
        $this->genericLoad('select * from sector', 'Crm\Model\Sector');
    }

}
