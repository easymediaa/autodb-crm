<?php

namespace Crm\Model\Content;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    private $arrRevisionCounter = null;

    public function loadAll() {
        $this->genericLoad('select id,alias,title,content,author,created from content where created = (select max(created) from content as f where f.alias = content.alias) order by created desc', __NAMESPACE__);
    }

    public function loadRevisions($strAlias) {
        $objStatement = $this->getDatasource()->prepare('select * from content where alias = :strAlias order by created desc');
        $objStatement->bindValue(':strAlias', $strAlias);
        $this->genericPreparedLoad($objStatement, __NAMESPACE__);
    }

    public function countRevisions($strAlias) {
        if (is_null($this->arrRevisionCounter)) {
            $this->arrRevisionCounter = array();
            $objStatement = $this->getDatasource()->query('select alias,count(*) as numberOfPosts from content group by alias');
            foreach ($objStatement->fetchAll() as $arrCounterData) {
                $this->arrRevisionCounter[$arrCounterData['alias']] = (int) $arrCounterData['numberOfPosts'];
            }
        }
        return isset($this->arrRevisionCounter[$strAlias]) ? $this->arrRevisionCounter[$strAlias] : 0;
    }

}
