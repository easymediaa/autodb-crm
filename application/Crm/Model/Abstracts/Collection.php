<?php

namespace Crm\Model\Abstracts;

use \workerbeeeu\autodb\Collection as AutodbCollection;
use \workerbeeeu\autodb\Traits\DatabaseObject;
use \workerbeeeu\autodb\Exception\DatabaseException;

abstract class Collection extends AutodbCollection {

    use DatabaseObject;

    abstract public function loadAll();

    public function __construct(\PDO $objDatasource) {
        $this->setDatasource($objDatasource);
    }

    protected function genericLoad($strQuery, $strObjectClassName) {
        $objStatement = $this->getDatasource()->prepare($strQuery);
        $this->genericPreparedLoad($objStatement, $strObjectClassName);
    }

    protected function genericPreparedLoad(\PDOStatement $objStatement, $strObjectClassName) {
        if ($objStatement->execute()) {
            while ($objDataset = $objStatement->fetchObject($strObjectClassName, array($this->getDatasource()))) {
                $this->attach($objDataset);
            }
        } else {
            throw new DatabaseException($objStatement->errorInfo()[2], $objStatement->errorCode());
        }
    }

}
