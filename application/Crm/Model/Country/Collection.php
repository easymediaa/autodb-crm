<?php

namespace Crm\Model\Country;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    public function loadAll() {
        $this->genericLoad('select * from country', 'Crm\Model\Country');
    }

}
