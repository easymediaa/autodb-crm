<?php

namespace Crm\Model;

use \workerbeeeu\autodb\Entity\Database\Blueprint;

/**
 * Abbildung der Benutzergruppen mit verschiedenen Rechten.
 *
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 *
 * @property int $id Primärschlüssel
 * @property string $name Name des Landes
 * @property string $beschreibung Beschreibung der Nutzerrechte
 */
class Access extends Blueprint {

    /**
     * Prüft, ob für die geladene Nutzergruppe eine Berechtigung besteht.
     * @param string $strAccessKey Schlüsselwert der Berechtigung
     * @return boolean True, wenn die Berechtigung erlaubt sind, sonst false
     */
    public function hasAccess($strAccessKey = 'superadmin') {
        return isset($this->{'access_' . $strAccessKey}) ? (bool) $this->{'access_' . $strAccessKey} : false;
    }

    /**
     * Setzt alle Rechte neu.
     */
    public function setAccessArray($arrNewRights) {
        $arrPosibleKeys = array_merge(array_keys($this->getObjectProperties()), array_keys($arrNewRights));
        foreach ($arrPosibleKeys as $strKey) {
            if (stripos($strKey, 'access_') === 0) {
                $this->$strKey = isset($arrNewRights[$strKey]) ? intval($arrNewRights[$strKey]) : 0;
            }
        }
    }

}
