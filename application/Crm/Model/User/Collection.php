<?php

namespace Crm\Model\User;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    public function loadAll() {
        $this->genericLoad('select * from user', 'Crm\Model\User');
    }

    public function count($boolCountAll = false) {
        $intCount = 0;
        if (!$boolCountAll) {
            /* @var $objUser \Crm\Model\User */
            foreach ($this as $objUser) {
                $intCount += $objUser->isDeleted() ? 0 : 1;
            }
        } else {
            $intCount = parent::count();
        }
        return $intCount;
    }

}
