<?php

namespace Crm\Model\Rel;

use Crm\Model\Abstracts\Collection as AbstractCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Collection extends AbstractCollection {

    public function setTable($strTable) {
        parent::setTable($strTable);
    }

    public function loadAll() {
        $this->genericLoad('select * from ' . $this->getTable(), 'Crm\Model\Rel');
    }

}
