<?php

namespace Crm\Model;

use \workerbeeeu\autodb\Entity\Database\Blueprint;

/**
 * Abbildung von statischen Inhalten.
 *
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 *
 * @property int $id Primärschlüssel
 */
class Content extends Blueprint {

    public function getDateCreated() {
        return new \DateTime($this->created);
    }

}
