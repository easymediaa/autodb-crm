<?php

namespace Crm\Blueprints\Template;

use workerbeeeu\autodb\Blueprints\Template as AutodbTemplate;

/**
 * Standard-Template für CRUD-Controller als Hilfe für die Autovervollständigung.
 *
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 *
 * @property string $strController Name des aktuellen Controllers
 * @property string $arrKeys Namen der Spalten, die angezeigt werden sollen
 * @property \workerbeeeu\autodb\Collection $objCollection Collection der Objekte in einem Listing
 * @property \workerbeeeu\autodb\Form\Generator $objForm Formular des aktuellen Objekts
 */
class Crud extends AutodbTemplate {
    
}
