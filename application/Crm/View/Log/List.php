<?php
/* @var $this \Crm\Blueprints\Template\Crud */
/* @var $objLog \workerbeeeu\autodb\Entity\Log */
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Log-File</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group btn-group-xs">
                    <a class="btn btn-default btn-danger" href="?controller=<?= $this->strController ?>&action=delete"><span class="glyphicon glyphicon-remove"></span> Leeren</a>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Datum</th>
                        <th>Zeit</th>
                        <th>Quelle</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($this->objCollection as $objLog) :
                        $strClass = "";
                        $strIco = "";
                        switch ($objLog->type) {
                            case E_USER_ERROR:
                                $strClass = "danger";
                                $strIco = "glyphicon glyphicon-fire";
                                break;
                            case E_USER_WARNING:
                                $strClass = "warning";
                                $strIco = "glyphicon glyphicon-flash";
                                break;
                            case E_USER_NOTICE:
                                $strClass = "info";
                                $strIco = "glyphicon glyphicon-bullhorn";
                                break;
                            case E_USER_DEPRECATED:
                                $strClass = "active";
                                $strIco = "glyphicon glyphicon-time";
                            default:
//                                $strClass = "info";
                        }
                        ?>
                        <tr class="<?= $strClass ?>">
                            <td><?= $objLog->id ?></td>
                            <td><?= $objLog->getDateCreated()->format('d.m.Y') ?></td>
                            <td><?= $objLog->getDateCreated()->format('H:i:s') ?> Uhr</td>
                            <td><?= $objLog->source ?></td>
                            <td><?= $strIco ? '<span class="' . $strIco . '"></span> ' : "" ?><?= $objLog->description ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>