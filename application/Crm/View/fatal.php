<?php
/* @var $exc Exception */
/* @var $Autodb \workerbeeeu\autodb*/
global $Autodb;
global $exc;
?><!doctype html>
<html>

    <head>
        <title>Fataler Ausnahmefehler</title>
        <base href="http://localhost/sternauto/" />
        <meta charset="utf-8" />
        <link href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
        <style>
            body {
                padding-top: 68px;
            }
            .page-header {
                margin-top: 9px;
            }
            .striked {
                text-decoration: line-through;
            }
            tr.striked {
                color: #aaa;
            }
            .navbar-nav .glyphicon {
                top: 2px;
                margin-right: 8px;
            }
        </style>
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="curator-navbar-collape">
                    <ul class="nav navbar-nav">
                        <li><a href="<?= $Autodb->getConfig()->getBaseUrl() ?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Ausnahmefehler</h1>
                    <div id="container">
                        <fieldset>
                            <p><i><?php echo get_class($exc); ?></i> in <i><?php echo $exc->getFile() ?></i>, line <i><?php echo $exc->getLine() ?></i></p>
                            <p><?php echo $exc->getMessage() ?></p>
                            <pre><?php echo $exc->getTraceAsString() ?></pre>
                        </fieldset>
                        <?php if ($exc->getPrevious()): ?>
                            <h2>Vorherige Meldungen</h2>
                            <?php
                            while ($exc = $exc->getPrevious()):
                                ?>
                                <fieldset>
                                    <p><i><?php echo get_class($exc); ?></i> in <i><?php echo $exc->getFile() ?></i>, line <i><?php echo $exc->getLine() ?></i></p>
                                    <p><?php echo $exc->getMessage() ?></p>
                                    <pre><?php echo $exc->getTraceAsString() ?></pre>
                                </fieldset>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <hr />
                        <p class="pull-right">
                            <a href="content/view/impressum">Impressum</a> |
                            <a href="content/view/datenschutz">Datenschutz</a>
                        </p>
                        <p>&copy; easymedia GmbH, 2016
                        </p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="assets/jquery-3.1.1.min.js"></script>
        <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <script src="assets/tinymce-4.4.3/js/tinymce/tinymce.min.js"></script>
        <script>tinymce.init({
                selector: 'textarea',
                language: 'de',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                init_instance_callback: function (editor) {
                    jQuery("#" + editor.id).removeAttr('required');
                    console.log("Editor: " + editor.id + " is now initialized.");
                }
            });</script>

    </body>

</html>