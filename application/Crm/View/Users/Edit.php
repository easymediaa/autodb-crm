<?php
/* @var $this \Crm\Blueprints\Template\Crud */
/* @var $objUser Sternauto\Model\User */
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Benutzer <small><?= is_null($this->objForm->getSourceEntity()->id) ? "erstellen" : $this->objForm->getSourceEntity()->name . " bearbeiten" ?></small></h1>
        <form action="?controller=<?= $this->strController ?>&amp;action=update" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Stammdaten</h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('id', 'name', 'email')) ?>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('anrede', 'vorname', 'nachname')) ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Passwort</h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('password')) ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Administration</h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('geloescht', 'access')) ?>
                </div>
                <div class="panel-footer">
                    <a class="btn btn-default btn-danger btn-sm" href="?controller=<?= $this->strController ?>"><span class="glyphicon glyphicon-remove"></span> Abbrechen und zurück</a>
                    <button class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon-<?= is_null($this->objForm->getSourceEntity()->id) ? "plus" : "ok" ?>"></span> <?= is_null($this->objForm->getSourceEntity()->id) ? "Erstellen" : "Speichern" ?></button>
                </div>
            </div>
        </form>
    </div>
</div>