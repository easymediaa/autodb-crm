<?php
/* @var $this \Crm\Blueprints\Template\Crud */
/* @var $objUser Sternauto\Model\User */
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Benutzer <span class="badge"><?= $this->objCollection->count() ?> / <?= $this->objCollection->count(true) ?></span> <small>Übersicht</small></h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group btn-group-xs">
                    <a class="btn btn-default btn-primary" href="?controller=<?= $this->strController ?>&action=edit"><span class="glyphicon glyphicon-plus"></span> Hinzufügen</a>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Anzeigename</th>
                        <th>Name</th>
                        <th>Berechtigung</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->objCollection as $objUser) : ?>
                        <tr class="<?= $objUser->geloescht ? 'striked' : '' ?>">
                            <td><?= $objUser->id ?></td>
                            <td><?= $objUser->name ?></td>
                            <td><?= $objUser->anrede ?> <?= $objUser->vorname ?> <?= $objUser->nachname ?></td>
                            <td><?= $objUser->getAccess()->name ?></td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <?php if ($objUser->geloescht): ?>
                                        <a class="btn btn-success btn-xs" href="?controller=<?= $this->strController ?>&action=reenable&primary=<?= $objUser->id ?>" title="Reaktivieren"><span class="glyphicon glyphicon-refresh"></span></a>
                                    <?php endif; ?>
                                    <a class="btn btn-default btn-xs<?= $objUser->geloescht ? ' disabled' : '' ?>" href="?controller=<?= $this->strController ?>&action=edit&primary=<?= $objUser->id ?>" title="Bearbeiten"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a class="btn btn-danger btn-xs<?= $objUser->geloescht ? ' disabled' : '' ?>" href="?controller=<?= $this->strController ?>&action=delete&primary=<?= $objUser->id ?>" title="Entfernen"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>