<?php
/* @var $objAccess Sternauto\Model\Access */
/* @var $this Sternauto\Blueprints\Template\Crud */
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Benutzergruppen <small>Übersicht</small></h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group btn-group-xs">
                    <a class="btn btn-primary" href="?controller=<?= $this->strController ?>&action=edit"><span class="glyphicon glyphicon-plus"></span> Hinzufügen</a>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Benutzergruppe</th>
                        <th>Beschreibung</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->objCollection as $objAccess) : ?>
                        <tr>
                            <td><?= $objAccess->id ?></td>
                            <td><?= $objAccess->name ?></td>
                            <td><?= $objAccess->beschreibung ?></td>
                            <td class="text-right">
                                <a class="btn btn-default btn-xs" href="?controller=<?= $this->strController ?>&action=edit&primary=<?= $objAccess->id ?>" title="Bearbeiten"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>