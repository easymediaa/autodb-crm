<!doctype html>
<html>

    <head>
        <base href="<?= $this->Config->getBaseUrl() ?>" />
        <title><?= $this->Config->getVar('routing', 'application') ?></title>
        <meta charset="utf-8" />
        <link href="lib/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet"></link>
        <style>
            body {
                padding-top: 68px;
            }
        </style>
    </head>

    <body>

        <div class="container-fluid">
            <?= $this->mainContent ?>
        </div>

        <script src="lib/jquery-2.1.4.min.js"></script>
        <script src="lib/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    </body>

</html>