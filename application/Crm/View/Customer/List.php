<?php
/* @var $objAccess \Crm\Model\Access */
/* @var $this \Crm\Blueprints\Template\Crud */
global $Autodb;

$objUserColl = new Crm\Model\User\Collection($Autodb->getPdo());
$objUserColl->loadAll();
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Kunden <small>Übersicht</small></h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group btn-group-xs">
                    <a class="btn btn-primary" href="?controller=<?= $this->strController ?>&action=edit"><span class="glyphicon glyphicon-plus"></span> Hinzufügen</a>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Firmenwortlaut</th>
                        <th>Telefon</th>
                        <th>E-Mail</th>
                        <th>Web</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->objCollection as $objContent) : ?>
                        <tr>
                            <td><?= $objContent->name ?></td>
                            <td><?= $objContent->firmenwortlaut ?></td>
                            <td><?= $objContent->telefon ? $objContent->telefon : "-" ?></td>
                            <td><a href="mailto:<?= $objContent->email ?>"><?= $objContent->email ?></a></td>
                            <td><?php
                                if ($objContent->web):
                                    ?>
                                    <a href="http://<?= $objContent->web ?>" target="_blank"><?= $objContent->web ?></a>
                                <?php else: ?>
                                    -
                                <?php
                                endif;
                                ?></td>
                            <td class="text-right">
                                <a class="btn btn-default btn-xs" href="?controller=<?= $this->strController ?>&action=edit&primary=<?= $objContent->id ?>" title="Bearbeiten"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>