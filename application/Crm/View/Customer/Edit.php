<?php
/* @var $this \Crm\Blueprints\Template\Crud */
/* @var $objUser Sternauto\Model\User */
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Kunden <small><?= is_null($this->objForm->getSourceEntity()->id) ? "erstellen" : $this->objForm->getSourceEntity()->name . " bearbeiten" ?></small></h1>
    </div>
    <form action="?controller=<?= $this->strController ?>&amp;action=update" method="post">
        <?= $this->objForm->generateFields(array('id')) ?>
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Stammdaten</h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('name', 'firmenwortlaut')) ?>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('firmengruendung', 'sector')) ?>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('anschrift_adresse', 'anschrift_hausnummer', 'anschrift_plz', 'anschrift_ort', 'anschrift_land')) ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Kontaktdaten</h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('telefon', 'telefax', 'email', 'web')) ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Ansprechpartner</h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $this->objForm->generateFields(array('erster_kontakt', 'kundenbetreuer')) ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <a class="btn btn-default btn-danger btn-sm" href="?controller=<?= $this->strController ?>"><span class="glyphicon glyphicon-remove"></span> Abbrechen und zurück</a>
                    <button class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon-<?= is_null($this->objForm->getSourceEntity()->id) ? "plus" : "ok" ?>"></span> <?= is_null($this->objForm->getSourceEntity()->id) ? "Erstellen" : "Speichern" ?></button>
                </div>
            </div>
        </div>
    </form>
</div>