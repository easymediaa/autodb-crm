<!doctype html>
<html>

    <head>
        <base href="<?= $this->Config->getBaseUrl() ?>" />
        <title><?= $this->Config->getVar('routing', 'application') ?></title>
        <meta charset="utf-8" />
        <link href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet"></link>
        <style>
            body {
                padding: 71px 0;
                background-color: #fafafa
            }
            .form-signin {
                max-width: 330px;
                margin-left: auto;
                margin-right: auto
            }
            input {
                margin-bottom: 8px
            }
        </style>
    </head>

    <body>
        <div class="container">

            <form class="form-signin" action="?controller=session&action=login" method="post" novalidate>
                <h2 class="form-signin-heading">Bitte anmelden</h2>
                <label for="inputEmail" class="sr-only">E-Mail-Addresse</label>
                <?php if ($this->failed): ?>
                    <div class="alert alert-warning">Anmeldung fehlgeschlagen</div>
                <?php endif ?>
                <input class="form-control" type="email" id="inputEmail" placeholder="E-Mail-Addresse" name="username" required autofocus>
                <label for="inputPassword" class="sr-only">Passwort</label>
                <input class="form-control" type="password" id="inputPassword" placeholder="Passwort" name="password" required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Angemeldet bleiben
                    </label>
                </div>
                <hr>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Anmelden</button>
            </form>

        </div> <!-- /container -->

        <script src="assets/jquery-2.1.4.min.js"></script>
        <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    </body>

</html>