<?php
/* @var $Autodb Autodb\workerbeeeu\autodb */
/* @var $objContent Sternauto\Model\Content */

global $Autodb;
?><div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Stern Auto Online-Portal</h1>
    </div>

    <div class="col-xs-12 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Hallo, <em><?= $this->objUser->vorname ?> <?= $this->objUser->nachname ?></em></h2>
            </div>
            <div class="panel-body">
                <?php if ($this->objStartUser['name']): ?>
                    <p>
                        <?= $this->objStartUser['name'] ?>
                    </p>
                <?php endif; ?>
                <?php if ($this->objUser->getAccess()->hasAccess('view_all_frontpage')): ?>
                    <form id="changeBranch" action="<?= $Autodb->getRequest() ?>" method="get">
                        <input name="controller" value="<?= $Autodb->getRequest()->getController() ?>" type="hidden" />
                        <div class="form-group">
                            <label class="control-label">Standort Vorschau</label>
                            <select class="form-control" name="branch_alias" onchange="getElementById('changeBranch').submit();">
                                <option value="0">Administrator</option>
                                <?php foreach ($this->arrBranchAliases as $arrBranch): ?>
                                    <option value="<?= $arrBranch['alias'] ?>"<?= $arrBranch['alias'] == $this->strBranchAlias ? " selected" : "" ?>><?= $arrBranch['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="help-block">Sie können mit der Auswahl des Standortes die jeweilige Startseite ansehen.</span>
                        </div>
                    </form>
                    <hr>
                    <ul class="nav nav-pills nav-stacked">
                        <li role="presentation"><a href="content/view/sternauto_ci">STERNAUTO CI Guidelines</a></li>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-8 col-lg-9">


    </div>

</div>