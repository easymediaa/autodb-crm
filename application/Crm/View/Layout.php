<?php
/* @var $Autodb \workerbeeeu\autodb\Autodb */
global $Autodb;
$objUser = $Autodb->getUser();
?><!doctype html>
<html>

    <head>
        <base href="<?= $this->Config->getBaseUrl() ?>" />
        <title><?= $this->Config->getVar('routing', 'application') ?></title>
        <meta charset="utf-8" />
        <link href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/app_sternauto.css" rel="stylesheet">
        <link href="assets/magnific-popup-1.1.0/dist/magnific-popup.css" rel="stylesheet">
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <!--<div class="navbar-header">
                                   <a href="<?= $this->Config->getBaseUrl() ?>" class="navbar-brand"><?= $this->Config->getVar('routing', 'application') ?></a>
                               </div>-->
                <div class="collapse navbar-collapse" id="curator-navbar-collape">
                    <ul class="nav navbar-nav">
                        <li><a href="<?= $this->Config->getBaseUrl() ?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-eye-open"></span> CRM <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <?php if ($objUser->getAccess()->hasAccess('customer_index')): ?>
                                    <li><a href="customer/"><span class="glyphicon glyphicon-piggy-bank"></span> Kunden</a></li>
                                <?php endif; ?>
                                <?php if ($objUser->getAccess()->hasAccess('counterpart_index')): ?>
                                    <li><a href="counterpart/"><span class="glyphicon glyphicon-user"></span> Ansprechpartner</a></li>
                                <?php endif; ?>
                                <?php if ($objUser->getAccess()->hasAccess('sector_index')): ?>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="sector/"><span class="glyphicon glyphicon-file"></span> Branchen</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-picture"></span> Inhalte <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <?php if ($objUser->getAccess()->hasAccess('content_index')): ?>
                                    <li><a href="content/"><span class="glyphicon glyphicon-align-left"></span> Statische Inhalte</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-cog"></span> System <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <?php if ($objUser->getAccess()->hasAccess('branch_index')): ?>
                                    <li><a href="branch/"><span class="glyphicon glyphicon-globe"></span> Niederlassungen</a></li>
                                    <li><a href="rel/?table=rel_branch_user"><span class="glyphicon glyphicon-link"></span> Benutzer &amp; Niederlassung</a></li>
                                    <li role="separator" class="divider"></li>
                                <?php endif; ?>
                                <?php if ($objUser->getAccess()->hasAccess('users_index')): ?>
                                    <li><a href="users/"><span class="glyphicon glyphicon-user"></span> Benutzer</a></li>
                                <?php endif; ?>
                                <?php if ($objUser->getAccess()->hasAccess('access_index')): ?>
                                    <li><a href="access/"><span class="glyphicon glyphicon-eye-close"></span> Benutzergruppen</a></li>
                                <?php endif; ?>
                                <?php if ($objUser->getAccess()->hasAccess('admin')): ?>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="filemanager/"><span class="glyphicon glyphicon-file"></span> Dateimanager</a></li>
                                    <li><a href="log/"><span class="glyphicon glyphicon-search"></span> Log-Files</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <li><a href="content/view/about">?</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-user"></span> Benutzer <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="session/?action=logout"><span class="glyphicon glyphicon-log-out"></span> Abmelden</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <?php Crm\Model\Alerts::output(); ?>
            <?= $this->mainContent ?>
        </div>

        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <hr />
                        <!--<p class="pull-right">
                            <a href="content/view/impressum">Impressum</a> |
                            <a href="content/view/datenschutz">Datenschutz</a>
                        </p>-->
                        <p>&copy; easymedia GmbH, 2016
                        </p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="assets/jquery-3.1.1.min.js"></script>
        <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <script src="assets/tinymce-4.5.3/js/tinymce/tinymce.min.js"></script>
        <script src="assets/tinymce-4.5.3/js/tinymce/jquery.tinymce.min.js"></script>
        <script src="assets/magnific-popup-1.1.0/dist/jquery.magnific-popup.min.js"></script>
        <script>

            function RoxyFileBrowser(field_name, url, type, win) {
                var roxyFileman = 'assets/roxy-fileman-1.4.5/index.html';
                if (roxyFileman.indexOf("?") < 0) {
                    roxyFileman += "?type=" + type;
                } else {
                    roxyFileman += "&type=" + type;
                }
                roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
                if (tinyMCE.activeEditor.settings.language) {
                    roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
                }
                tinyMCE.activeEditor.windowManager.open({
                    file: roxyFileman,
                    title: 'Roxy Fileman',
                    width: 850,
                    height: 650,
                    resizable: "yes",
                    plugins: "media",
                    inline: "yes",
                    close_previous: "no"
                }, {window: win, input: field_name});
                return false;
            }

            (function ($) {
<?php if ($objUser->getAccess()->hasAccess('admin')): ?>
                    if ($('#filemanager').length) {
                        $(window).resize(function () {
                            $('#filemanager').height($(window).height() - $('footer').outerHeight() - $('body > .navbar').outerHeight() - 22);
                        }).resize();
                    }
<?php endif; ?>
                if ($('textarea').length) {
                    $('textarea').tinymce({
                        selector: 'textarea',
                        language: 'de',
                        plugins: [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace wordcount visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools'
                        ],
                        init_instance_callback: function (editor) {
                            jQuery("#" + editor.id).removeAttr('required');
                            console.log("Editor: " + editor.id + " is now initialized.");
                        },
                        file_browser_callback: RoxyFileBrowser,
                        height: "380"
                    });
                }

                // Magnific Popup
                // configs
                $arrMagnificPopupSettings = {
                    type: 'image',
                    mainClass: 'mfp-with-zoom mfp-img-mobile',
                    tClose: 'Schließen (Esc)',
                    image: {
                        verticalFit: true,
                        titleSrc: function (item) {
                            elem = $(item.el[0]).find('img').first();
                            return elem.attr('title') ? elem.attr('title') : elem.attr('alt');
                        }
                    }
                };

//                $('a.popup').magnificPopup($arrMagnificPopupSettings);

                $arrMagnificPopupGallerySettings = $.extend($arrMagnificPopupSettings, {
                    delegate: 'a.popup, a[href$=".jpg"], a[href$=".png"]',
                    gallery: {
                        enabled: true,
                        tPrev: 'zurück ',
                        tNext: 'weiter',
                        tCounter: '%curr% von %total%'
                    }
                });

                $('.panel-body').each(function (idx, obj) {
                    $(obj).magnificPopup($arrMagnificPopupGallerySettings);
                });
            })(jQuery);
        </script>

    </body>

</html>