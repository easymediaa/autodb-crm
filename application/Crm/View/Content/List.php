<?php
/* @var $objAccess \Crm\Model\Access */
/* @var $this \Crm\Blueprints\Template\Crud */
global $Autodb;

$objUserColl = new Crm\Model\User\Collection($Autodb->getPdo());
$objUserColl->loadAll();
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Statische Inhalte <small>Übersicht</small></h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group btn-group-xs">
                    <a class="btn btn-primary" href="?controller=<?= $this->strController ?>&action=edit"><span class="glyphicon glyphicon-plus"></span> Hinzufügen</a>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Titel</th>
                        <th>Letzte Änderung</th>
                        <th>Letzte Änderung durch</th>
                        <th>Revision</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($this->objCollection as $objContent) :
                        $objUser = $objUserColl->getByField($objContent->author);
                        ?>
                        <tr>
                            <td class="h5"><?= $objContent->title ?> <small>(<?= $objContent->alias ?>)</small></td>
                            <td><?= $objContent->getDateCreated()->format("d.m.Y, H:i:s") ?> Uhr</td>
                            <td><?= $objUser->name ?></td>
                            <td>#<?= str_pad($this->objCollection->countRevisions($objContent->alias), 4, 0, STR_PAD_LEFT) ?></td>
                            <td class="text-right">
                                <a class="btn btn-default btn-xs" href="?controller=<?= $this->strController ?>&action=edit&primary=<?= $objContent->id ?>" title="Bearbeiten"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>