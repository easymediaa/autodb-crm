<?php
/* @var $objContent Sternauto\Model\Content */
$objContent = $this->objContent;
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header"><?= $objContent->title ?></h1>
        <?= $objContent->content ?>
    </div>
    <div class="col-xs-12">
        <br>
        <div class="panel panel-default">
            <div class="panel-body">
                Zuletzt geändert am <em><?= $objContent->getDateCreated()->format('d.m.Y') ?></em>, um  <em><?= $objContent->getDateCreated()->format('H:i:s') ?> Uhr</em>
            </div>
        </div>
    </div>
</div>