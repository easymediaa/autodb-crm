<?php
global $Autodb;
/* @var $objForm Autodb\Form\Generator */
/* @var $this \Crm\Blueprints\Template\Crud */
$objForm = $this->objForm;
// Verhindert das bearbeiten und übernehmen der Beschreibung
$objForm->getSourceEntity()->revision_desc = "";
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Statischer Inhalt</h1>
        <form action="?controller=<?= $this->strController ?>&amp;action=update" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><?= is_null($objForm->getSourceEntity()->id) ? "Inhalt erstellen" : "Inhalt bearbeiten" ?></h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $objForm->generate(array('created', 'author')) ?>
                </div>
                <div class="panel-footer">
                    <a class="btn btn-default btn-danger btn-sm" href="?controller=<?= $this->strController ?>"><span class="glyphicon glyphicon-remove"></span> Abbrechen und zurück</a>
                    <button class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon-<?= is_null($objForm->getSourceEntity()->id) ? "plus" : "ok" ?>"></span> <?= is_null($objForm->getSourceEntity()->id) ? "Erstellen" : "Speichern" ?></button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Revisionen</h2>
            </div>
            <div class="panel-body">
                <p>
                    Diese Übersicht zeigt ale noch verfügbaren Versionen dieser Seite und dessen Autoren.
                </p>
            </div>
            <?php
            global $Autodb;
            $objRevisionTemplate = new \workerbeeeu\autodb\Blueprints\Template($Autodb->getConfig());
            $objRevisionTemplate->setTemplatePath('Shared/RevisionTableWithAuthor');
            $objRevisionTemplate->strController = $this->strController;
            $objRevisionTemplate->objRevisions = $this->objRevisions;
            $objRevisionTemplate->display();
            ?>
        </div>
    </div>
</div>