<?php
global $Autodb;
?>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Änderung</th>
            <th>Änderung durch</th>
            <th>Änderungsvermerk</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($this->objRevisions->count()) :
            $objUserColl = new Crm\Model\User\Collection($Autodb->getPdo());
            $objUserColl->loadAll();
            foreach ($this->objRevisions as $objContent) :
                $objUser = $objUserColl->getByField($objContent->author);
                ?>
                <tr>
                    <td><?= $objContent->getDateCreated()->format("d.m.Y, H:i:s") ?> Uhr</td>
                    <td><?= $objUser->name ?></td>
                    <td><?= $objContent->revision_desc ? $objContent->revision_desc : "-" ?></td>
                    <td class="text-right">
                        <a class="btn btn-default btn-xs" href="?controller=<?= $this->strController ?>&action=edit&primary=<?= $objContent->id ?>" title="Diese Revision benutzen"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                </tr>
                <?php
            endforeach;
        else:
            ?>
            <tr>
                <td colspan="3" class="text-center">Keine weiteren Revisionen verfügbar.</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>