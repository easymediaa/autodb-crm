<?php
/* @var $objForm Autodb\Form\Generator */
/* @var $this \Crm\Blueprints\Template\Crud */
$objForm = $this->objForm;
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Verknüpfung <small>für <?= $this->strTableName ?></small></h1>
        <form action="?controller=<?= $this->strController ?>&amp;action=update&amp;table=<?= $this->strTableName ?>" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Relation <?= is_null($objForm->getSourceEntity()->id) ? "erstellen" : "bearbeiten" ?></h2>
                </div>
                <div class=" panel-body form-horizontal">
                    <?= $objForm->generate() ?>
                </div>
                <div class="panel-footer">
                    <a class="btn btn-default btn-danger btn-sm" href="?controller=<?= $this->strController ?>&amp;table=<?= $this->strTableName ?>"><span class="glyphicon glyphicon-remove"></span> Abbrechen und zurück</a>
                    <button class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon-<?= is_null($objForm->getSourceEntity()->id) ? "plus" : "ok" ?>"></span> <?= is_null($objForm->getSourceEntity()->id) ? "Erstellen" : "Speichern" ?></button>
                </div>
            </div>
        </form>
    </div>
</div>