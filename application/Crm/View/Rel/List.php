<?php
/* @var $this \Crm\Blueprints\Template\Crud */
/* @var $objClass \Crm\Model\Abstracts\Collection */
$arrCollections = array();
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Verknüpfung <small>Übersicht für <?= $this->strTableName ?></small></h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group btn-group-xs">
                    <a class="btn btn-primary" href="?controller=<?= $this->strController ?>&amp;action=edit&amp;table=<?= $this->strTableName ?>"><span class="glyphicon glyphicon-plus"></span> Hinzufügen</a>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <?php
                        foreach ($this->objCollection as $objContent) :
                            foreach (array_keys($objContent->getObjectProperties()) as $strKey) :
                                if ($strKey !== "id"):
                                    $strClassName = 'Crm\\Model\\' . self::stringToObjectName(substr($strKey, 0, strrpos($strKey, "_"))) . '\\Collection';
                                    $arrCollections[$strKey] = new $strClassName($this->objPdo);
                                    $arrCollections[$strKey]->loadAll();
                                endif;
                                ?>
                                <th><?= $strKey == 'id' ? '#' : $strKey ?></th>
                                <?php
                            endforeach;
                            break;
                        endforeach;
                        ?>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->objCollection as $objContent) : ?>
                        <tr>
                            <?php
                            foreach ($objContent->getObjectProperties() as $strKey => $mixedValue) :
                                if (isset($arrCollections[$strKey])):
                                    $strAccessKey = substr($strKey, strrpos($strKey, "_") + 1);
                                    $objItem = $arrCollections[$strKey]->getByField($mixedValue, $strAccessKey);
                                    ?>
                                    <td><?= $objItem->name ?></td>
                                    <?php
                                else:
                                    ?>
                                    <td><?= $mixedValue ?></td>
                                <?php
                                endif;
                                ?>
                            <?php endforeach; ?>
                            <td class="text-right">
                                <a class="btn btn-default btn-xs" href="?controller=<?= $this->strController ?>&amp;action=edit&amp;table=<?= $this->strTableName ?>&amp;primary=<?= $objContent->id ?>" title="Bearbeiten"><span class="glyphicon glyphicon-pencil"></span></a>                                
                                <a class="btn btn-danger btn-xs" href="?controller=<?= $this->strController ?>&action=delete&amp;table=<?= $this->strTableName ?>&amp;primary=<?= $objContent->id ?>" title="Entfernen"><span class="glyphicon glyphicon-remove"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>