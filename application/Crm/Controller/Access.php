<?php

namespace Crm\Controller;

use Crm\Controller\Abstracts\Crud;
use Crm\Model\Access as DatabaseModel;
use Crm\Model\Access\Collection as DatabaseModelCollection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Access extends Crud {

    public function actionIndex() {
        $objAccess = new DatabaseModelCollection($this->pdo());
        $objAccess->loadAll();
        $this->abstractIndex($this->strControllerName . "/List", $objAccess);
    }

    public function actionEdit() {
        $this->abstractEdit($this->strControllerName . "/Edit", new DatabaseModel($this->pdo()));
    }

    public function actionUpdate() {
        // erste Etappe sind die Grundinfomrationen der Berechtigung
        $arrPostData = filter_input_array(INPUT_POST, array(
            "id" => FILTER_VALIDATE_INT,
            "name" => FILTER_SANITIZE_STRING,
            "beschreibung" => FILTER_SANITIZE_STRING
        ));

        $objToUpdate = new DatabaseModel($this->pdo());
        if ($arrPostData['id']) {
            $objToUpdate->load($arrPostData['id']);
        }
        // zweite Etappe sind die Einzelberechtigungen
        $objToUpdate->setAccessArray(filter_input_array(INPUT_POST));
        $this->abstractUpdate($objToUpdate, filter_input_array(INPUT_POST, $arrPostData));
    }

    public function actionDelete() {
        throw new \LogicException('It is not allowed to delete access!', E_USER_WARNING);
    }

}
