<?php

namespace Crm\Controller;

use workerbeeeu\autodb\Controller\Blueprint as ControllerBlueprint;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Dashboard extends ControllerBlueprint {

    protected $objTemplate;

    public function __construct(\workerbeeeu\autodb\Autodb $Autodb) {
        parent::__construct($Autodb);
        $this->requireActiveSession();

        $this->objTemplate = new \workerbeeeu\autodb\Blueprints\Template($this->autodb()->getConfig());
    }

    public function actionIndex() {
        $this->objTemplate->setTemplatePath('Dashboard');

        $objStatement = $this->pdo()->prepare("SELECT user.id,user.anrede,user.vorname,user.nachname,user.email,branch.alias,branch.name FROM rel_branch_user right join branch on branch.id = rel_branch_user.branch_id right join user on user.id = rel_branch_user.user_id WHERE user.id = ?");
        $objStatement->execute(array(
            $this->user()->id));

        $this->objTemplate->objStartUser = $objStatement->fetch(\PDO::FETCH_ASSOC);
        $this->objTemplate->objUser = $this->user();

        $this->page()->mainContent = $this->objTemplate;
    }

    /**
     * Lädt einen Inhaltsbereich anhand seines Alias.
     * @todo das gehört an das Model?
     * @param string $strAlias
     * @return boolean|Sternauto\Model\Content
     */
    private function loadContentByAlias($strAlias) {
        $objStatement = $this->pdo()->prepare("SELECT * FROM content WHERE alias = :strAlias order by created desc limit 1");
        $objStatement->bindParam(':strAlias', $strAlias, \PDO::PARAM_STR);
        $objStatement->execute();
        return $objStatement->fetchObject('Crm\Model\Content', array($this->pdo()));
    }

}
