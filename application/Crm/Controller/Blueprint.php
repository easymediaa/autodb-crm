<?php

namespace Crm\Controller;

use workerbeeeu\autodb\Autodb;
use workerbeeeu\autodb\Controller\Blueprint as AutodbBlueprint;
use workerbeeeu\autodb\Blueprints\Template;

/**
 * Neuer Controller, zur Vererbung von Standardfunktionen.
 *
 * @author Maik Lampe <ml@easy-media.de>
 */
abstract class Blueprint extends AutodbBlueprint {

    /**
     * @var \workerbeeeu\autodb\Blueprints\Template
     */
    protected $objTemplate;
    protected $strControllerName;

    public function __construct(Autodb $Autodb) {
        parent::__construct($Autodb);
        $this->requireActiveSession();
        $this->requireControllerAccess();

        $arrClass = explode("\\", get_called_class());

        $this->objTemplate = new Template($this->autodb()->getConfig());
        $this->strControllerName = end($arrClass);
        $this->objTemplate->strController = $this->strControllerName;
    }

    protected function addAlert(Alert $objAlert) {
        Alerts::attach($objAlert);
    }

    protected function requireControllerAccess() {
        $strAccesskey = strtolower($this->autodb()->getRequest()->getController() . "_" . $this->autodb()->getRequest()->getAction());
        $this->requireAccess($strAccesskey);
    }

    protected function requireAccess($strAccesskey) {
        if (!$this->user()->getAccess()->hasAccess($strAccesskey)) {
            $this->autodb()->redirect(array(
                'controller' => 'session',
                'action' => 'denied'
            ));
        }
    }

}
