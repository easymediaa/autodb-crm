<?php

namespace Crm\Controller;

use workerbeeeu\autodb\Controller\Session as ControllerSessionBlueprint;
use workerbeeeu\autodb\Blueprints\Template;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Session extends ControllerSessionBlueprint {

    public function actionIndex() {
        if ($this->user()->isLoggedIn()) {
            $this->autodb()->redirect(array('controller' => 'dashboard'));
        } else {
            $this->showLoginForm();
        }
    }

    public function actionDenied() {
        header(filter_input(INPUT_SERVER, "SERVER_PROTOCOL") . ' 403 Forbidden');
        $objTemplate = new Template($this->autodb()->getConfig());
        $objTemplate->setTemplatePath('Session/403');
        $this->page()->mainContent = $objTemplate;
    }

    protected function showLoginForm() {
        $this->autodb()->getPage()->setTemplatePath('Session/Login');
    }

}
