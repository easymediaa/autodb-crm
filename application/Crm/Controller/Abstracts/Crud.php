<?php

namespace Crm\Controller\Abstracts;

use workerbeeeu\autodb\Autodb;
use workerbeeeu\autodb\Collection;
use workerbeeeu\autodb\Controller\Blueprint as ControllerBlueprint;
use workerbeeeu\autodb\Form\Generator;
use workerbeeeu\autodb\Entity\Log;
use workerbeeeu\autodb\Entity\Database\Blueprint as DatabaseModel;
use Crm\Model\Alert;
use Crm\Model\Alerts;
use Crm\Blueprints\Template\Crud as Template;

/**
 * Ein generischer Create/Insert/Update/Delete-Controller für schnelleres
 * Hinzufügen von neuen Eingabemasken und Listings.
 *
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 * @method \Crm\Model\User user()
 */
abstract class Crud extends ControllerBlueprint {

    /**
     * @var \workerbeeeu\autodb\Blueprints\Template
     */
    protected $objTemplate;
    protected $strControllerName;

    public function __construct(Autodb $Autodb) {
        parent::__construct($Autodb);
        $this->requireActiveSession();
        $this->requireControllerAccess();

        $arrClass = explode("\\", get_called_class());

        $this->objTemplate = new Template($this->autodb()->getConfig());
        $this->strControllerName = end($arrClass);
        $this->objTemplate->strController = $this->strControllerName;
    }

    abstract function actionEdit();

    abstract function actionDelete();

    /**
     * Fügt die gewünschte Sammlung in das vorgegebene Template ein.
     * @param string $strTpl Pfad der Vorlage
     * @param \Crm\Model\Abstracts\Collection objCollection Sammlung mit den anzuzeigenden Objekten.
     */
    protected function abstractIndex($strTpl, Collection $objCollection) {
        $this->objTemplate->setTemplatePath($strTpl);
        $this->objTemplate->objCollection = $objCollection;

        $this->autodb()->getPage()->mainContent = $this->objTemplate;
    }

    /**
     * Lädt ein voreingestelltes Objekt in ein Template.
     * @param string $strTpl Pfad der Vorlage
     * @param workerbeeeu\autodb\Entity\Database\Blueprint $objToEdit Das anzuzeigende Objekt.
     */
    protected function abstractEdit($strTpl, DatabaseModel $objToEdit) {
        $this->objTemplate->setTemplatePath($strTpl);

        $objForm = new Generator($objToEdit);
        try {
            $intPrimary = filter_input(INPUT_GET, "primary", FILTER_VALIDATE_INT);
            $objToEdit->load($intPrimary);
        } catch (\LogicException $ex) {
            // @todo create new setting
        }
        $this->objTemplate->objForm = $objForm;

        $this->page()->mainContent = $this->objTemplate;
    }

    /**
     * @param DatabaseModel $objToDelete
     * @param integer $intPrimary
     * @param string $strActionAfter
     */
    protected function abstractDelete(DatabaseModel $objToDelete, $intPrimary, $strActionAfter = null) {
        $objAlert = new Alert();
        $strObjectToDeleteClass = get_class($objToDelete);
        $objAlert->addArgument(1, $strObjectToDeleteClass);
        try {
            $objToDelete->load($intPrimary);
            $intId = $objToDelete->id;
            $objToDelete->delete();
            $objAlert->addArgument(2, $intId);
            $objAlert->setMessage('%s #%d erfolgreich gelöscht.');
            Log::log(get_called_class() . "::" . __FUNCTION__ . "()", get_class($objToDelete) . " #" . $intId . " deleted.");
        } catch (\LogicException $ex) {
            $objAlert->addArgument(2, $objToDelete->id ? $objToDelete->id : "unknown");
            $objAlert->setMessage('%s #%d konnte nicht als gelöscht markiert werden.');
            $objAlert->setType(Alert::TYPE_DANGER);
            Log::log(get_called_class() . "::" . __FUNCTION__ . "()", $ex->getMessage(), E_USER_ERROR);
        }
        $this->addAlert($objAlert);
        if ($strActionAfter !== false) {
            $this->autodb()->redirect(array(
                'controller' => $this->autodb()->getRequest()->getController(),
                'action' => $strActionAfter ? $strActionAfter : 'index'
            ));
        }
    }

    /**
     * @param \workerbeeeu\autodb\Entity\Database\Blueprint $objToUpdate
     * @param type $arrDataForUpdate
     */
    protected function abstractUpdate(DatabaseModel $objToUpdate, $arrDataForUpdate, $boolRedirectToEdit = true) {
        $objAlert = new Alert();
        $strObjectToEditClass = get_class($objToUpdate);
        $objAlert->addArgument(1, $strObjectToEditClass);
        try {
            foreach ($arrDataForUpdate as $strFieldname => $mixedFieldValue) {
                $objToUpdate->$strFieldname = $mixedFieldValue;
            }
            $objToUpdate->persist();
            $objAlert->addArgument(2, $objToUpdate->id);
            $objAlert->setMessage('%s #%d wurde erfolgreich aktualisiert/erstellt.');
        } catch (\LogicException $ex) {
            $objAlert->addArgument(2, $objToUpdate->id ? $objToUpdate->id : "unknown");
            $objAlert->setMessage('%s #%d konnte nicht als aktualisiert/erstellt werden.');
            $objAlert->setType(Alert::TYPE_DANGER);
            Log::log(get_called_class() . "::" . __FUNCTION__ . "()", $ex->getMessage(), E_USER_ERROR);
        }
        $this->addAlert($objAlert);
        if ($boolRedirectToEdit) {
            $this->autodb()->redirect(array(
                'controller' => $this->autodb()->getRequest()->getController(),
                'action' => 'edit',
                'primary' => $objToUpdate->id
            ));
        }
    }

    protected function addAlert(Alert $objAlert) {
        Alerts::attach($objAlert);
    }

    protected function requireControllerAccess() {
        $strAccesskey = strtolower($this->autodb()->getRequest()->getController() . "_" . $this->autodb()->getRequest()->getAction());
        $this->requireAccess($strAccesskey);
    }

    protected function requireAccess($strAccesskey) {
        if (!$this->user()->getAccess()->hasAccess($strAccesskey)) {
            $this->autodb()->redirect(array(
                'controller' => 'session',
                'action' => 'denied'
            ));
            exit;
        }
    }

}
