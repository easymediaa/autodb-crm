<?php

namespace Crm\Controller;

use Crm\Controller\Blueprint;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Filemanager extends Blueprint {

    public function actionIndex() {
        $this->objTemplate->setTemplatePath('Filemanager/Index');
        $this->page()->mainContent = $this->objTemplate;
    }

}
