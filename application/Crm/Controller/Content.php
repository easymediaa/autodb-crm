<?php

namespace Crm\Controller;

use Crm\Controller\Abstracts\Crud;
use Crm\Model\Content as ContentModel;
use Crm\Model\Content\Collection as DatabaseModelCollection;
use workerbeeeu\autodb\Exception\DatabaseException;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 * @todo Versionierung einführen
 */
class Content extends Crud {

    public function actionIndex() {
        $objContent = new DatabaseModelCollection($this->pdo());
        $objContent->loadAll();
        $this->abstractIndex($this->strControllerName . "/List", $objContent);
    }

    public function actionEdit() {
        $objContent = new ContentModel($this->pdo());

        $this->abstractEdit($this->strControllerName . "/Edit", $objContent);

        $objContentRevisionCollection = new DatabaseModelCollection($this->pdo());
        $objContentRevisionCollection->loadRevisions($objContent->alias);

        $this->objTemplate->objRevisions = $objContentRevisionCollection;
    }

    public function actionUpdate() {
        $objToUpdate = new ContentModel($this->pdo());
        // Datum vorlegeben
        $objToUpdate->created = date("Y_m-d H:i:s");
        $objToUpdate->author = $this->user()->id;
        $this->abstractUpdate($objToUpdate, filter_input_array(INPUT_POST, array(
            "alias" => FILTER_SANITIZE_STRING,
            "title" => FILTER_SANITIZE_STRING,
            "revision_desc" => FILTER_SANITIZE_STRING,
            "content" => FILTER_UNSAFE_RAW
        )));
    }

    public function actionDelete() {
        throw new \LogicException('It is not allowed to delete content!', E_USER_WARNING);
    }

    public function actionView() {
        $this->objTemplate->setTemplatePath($this->strControllerName . "/View");
        try {
            $strAlias = filter_input(INPUT_GET, 'alias');
            $objStatement = $this->pdo()->query("select max(id) as id from content where alias = '" . $strAlias . "'");
            if ($objStatement) {
//                $objStatement->execute();
                $intId = $objStatement->fetchColumn(0);
                if (!is_null($intId)) {
                    $objContent = new ContentModel($this->pdo());
                    $objContent->load($intId);
                    $this->objTemplate->objContent = $objContent;
                } else {
                    throw new DatabaseException('Content with alias <em>' . $strAlias . '</em> not found!', E_USER_WARNING);
                }
            }
        } catch (DatabaseException $ex) {
            header(filter_input(INPUT_SERVER, "SERVER_PROTOCOL") . ' 404 Not Found');
            $this->objTemplate->setTemplatePath('Content/404');
        }
        $this->page()->mainContent = $this->objTemplate;
    }

}
