<?php

namespace Crm\Controller;

use Crm\Controller\Abstracts\Crud;
use Crm\Model\Rel as RelModel;
use Crm\Model\Rel\Collection as Collection;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Rel extends Crud {

    public function __construct(\workerbeeeu\autodb\Autodb $Autodb) {
        parent::__construct($Autodb);
        $strTableName = filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING);
        $this->requireAccess('rel_' . $strTableName);
    }

    public function actionIndex() {
        $this->requireControllerAccess();
        $strTableName = filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING);

        $objContent = new Collection($this->pdo());
        $objContent->setTable($strTableName);
        $objContent->loadAll();

        $this->objTemplate->objPdo = $this->autodb()->getPdo();
        $this->objTemplate->strTableName = $strTableName;
        $this->abstractIndex("Rel/List", $objContent);
    }

    public function actionEdit() {
        $this->requireControllerAccess();
        $strTableName = filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING);
        $intPrimary = filter_input(INPUT_GET, 'primary', FILTER_VALIDATE_INT);

        $objRel = new RelModel($this->pdo());
        $objRel->setTable($strTableName);
        if ($intPrimary) {
            $objRel->load($intPrimary);
        }

        $this->objTemplate->strTableName = $strTableName;
        $this->abstractEdit("Rel/Edit", $objRel);
    }

    public function actionUpdate() {
        $this->requireControllerAccess();
        $strTableName = filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING);
        $objToUpdate = new RelModel($this->pdo());
        $objToUpdate->setTable($strTableName);
        $this->abstractUpdate($objToUpdate, filter_input_array(INPUT_POST), false);
        $this->autodb()->redirect(array(
            'controller' => $this->autodb()->getRequest()->getController(),
            'action' => 'edit',
            'primary' => $objToUpdate->id,
            'table' => $strTableName
        ));
    }

    public function actionDelete() {
        $this->requireControllerAccess();

        $intPrimary = filter_input(INPUT_GET, "primary", FILTER_VALIDATE_INT);
        $strTableName = filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING);

        $objToDelete = new RelModel($this->pdo());
        $objToDelete->setTable($strTableName);

        $this->abstractDelete($objToDelete, $intPrimary, false);
        $this->autodb()->redirect(array(
            'controller' => $this->autodb()->getRequest()->getController(),
            'action' => 'index',
            'table' => $strTableName
        ));
    }

}
