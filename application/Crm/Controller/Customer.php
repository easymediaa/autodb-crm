<?php

namespace Crm\Controller;

use Crm\Controller\Abstracts\Crud;
use Crm\Model\Customer as ContentModel;
use Crm\Model\Customer\Collection as DatabaseModelCollection;
use workerbeeeu\autodb\Exception\DatabaseException;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 * @todo Versionierung einführen
 */
class Customer extends Crud {

    public function actionIndex() {
        $objContent = new DatabaseModelCollection($this->pdo());
        $objContent->loadAll();
        $this->abstractIndex($this->strControllerName . "/List", $objContent);
    }

    public function actionEdit() {
        $objContent = new ContentModel($this->pdo());
        $this->abstractEdit($this->strControllerName . "/Edit", $objContent);
    }

    public function actionUpdate() {
        $objToUpdate = new ContentModel($this->pdo());
        // Datum korrekt einaprsen
        $arrPostData = filter_input_array(INPUT_POST, array(
            "id" => FILTER_VALIDATE_INT,
            "name" => FILTER_SANITIZE_STRING,
            "firmenwortlaut" => FILTER_SANITIZE_STRING,
            "firmengruendung" => FILTER_SANITIZE_STRING,
            "anschrift_adresse" => FILTER_SANITIZE_STRING,
            "anschrift_hausnummer" => FILTER_SANITIZE_STRING,
            "anschrift_plz" => FILTER_SANITIZE_STRING,
            "anschrift_ort" => FILTER_SANITIZE_STRING,
            "anschrift_land" => FILTER_VALIDATE_INT,
            "telefon" => FILTER_SANITIZE_STRING,
            "telefax" => FILTER_SANITIZE_STRING,
            "email" => FILTER_VALIDATE_EMAIL,
            "web" => FILTER_SANITIZE_STRING,
            "erster_kontakt" => FILTER_SANITIZE_STRING,
            "kundenbetreuer" => FILTER_VALIDATE_INT,
            "sector" => FILTER_VALIDATE_INT
        ));

        $objDateTime = \DateTime::createFromFormat('Y-m-d', filter_input(INPUT_POST, 'firmengruendung'));
        $objDateTime->setTime(0, 0, 1);
        $arrPostData['firmengruendung'] = $objDateTime->format('Y-m-d');

        $objDateTime = \DateTime::createFromFormat('Y-m-d', filter_input(INPUT_POST, 'erster_kontakt'));
        $objDateTime->setTime(0, 0, 1);
        $arrPostData['erster_kontakt'] = $objDateTime->format('Y-m-d');

        $this->abstractUpdate($objToUpdate, $arrPostData);
    }

    public function actionDelete() {
        throw new \LogicException('It is not allowed to delete customers!', E_USER_WARNING);
    }

}
