<?php

namespace Crm\Controller;

use workerbeeeu\autodb\Entity\Log as LogFile;
use Crm\Controller\Abstracts\Crud;
use Crm\Model\Log\Collection;
use Crm\Model\Alert;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 * @todo Versionierung einführen
 */
class Log extends Crud {

    public function actionIndex() {
        $objContent = new Collection($this->pdo());
        $objContent->loadAll();
        $this->abstractIndex("Log/List", $objContent);
    }

    public function actionDelete() {
        $objAlert = new Alert();
        try {
            $this->pdo()->query('TRUNCATE log');
            $objAlert->setMessage('Das Logfile wurde zurückgesetzt.');
            LogFile::log(get_called_class() . "::" . __FUNCTION__ . "()", "All deleted.");
        } catch (\LogicException $ex) {
            $objAlert->setMessage('Das Logfile konnte nicht gelöscht werden.');
            $objAlert->setType(Alert::TYPE_DANGER);
            LogFile::log(get_called_class() . "::" . __FUNCTION__ . "()", $ex->getMessage(), E_USER_ERROR);
        }
        $this->addAlert($objAlert);
        $this->autodb()->redirect(array(
            'controller' => $this->autodb()->getRequest()->getController()
        ));
    }

    public function actionEdit() {
        
    }

}
