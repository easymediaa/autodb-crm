<?php

namespace Crm\Controller;

use Crm\Controller\Abstracts\Crud;
use Crm\Model\User;
use Crm\Model\User\Collection;
use Crm\Model\Alert;

/**
 * @author Maik Lampe <maik.lampe@workerbee.eu>
 */
class Users extends Crud {

    public function actionIndex() {
        $objTaxes = new Collection($this->pdo());
        $objTaxes->loadAll();
        $this->abstractIndex($this->strControllerName . "/List", $objTaxes);
    }

    public function actionEdit() {
        $this->abstractEdit($this->strControllerName . "/Edit", new User($this->pdo()));
    }

    public function actionUpdate() {
        $this->abstractUpdate(new User($this->pdo()), filter_input_array(INPUT_POST, array(
            "id" => FILTER_VALIDATE_INT,
            "name" => FILTER_SANITIZE_STRING,
            "email" => FILTER_VALIDATE_EMAIL,
            "anrede" => FILTER_SANITIZE_STRING,
            "vorname" => FILTER_SANITIZE_STRING,
            "nachname" => FILTER_SANITIZE_STRING,
            "password" => FILTER_UNSAFE_RAW,
            "access" => FILTER_VALIDATE_INT,
        )));
    }

    public function actionDelete() {
        $intPrimary = filter_input(INPUT_GET, "primary", FILTER_VALIDATE_INT);
        $this->abstractDelete(new User($this->pdo()), $intPrimary);
    }

    public function actionReenable() {
        $this->requireControllerAccess();
        $objUser = new User($this->pdo());
        $intPrimary = filter_input(INPUT_GET, "primary", FILTER_VALIDATE_INT);
        $objAlert = new Alert();
        $objAlert->addArgument(1, "#" . $intPrimary);
        try {
            $objUser->load($intPrimary);
            $objUser->geloescht = 0;
            $objUser->persist();
            $objAlert->setMessage('Benutzer %s erfolgreich wiederhergestellt.');
        } catch (\LogicException $ex) {
            $objAlert->setMessage('Benutzer %s konnte nicht wiederhergestellt werden.');
            $objAlert->setType(Alert::TYPE_DANGER);
        }
        $this->addAlert($objAlert);
        $this->autodb()->redirect(array(
            'controller' => $this->autodb()->getRequest()->getController()
        ));
    }

}
