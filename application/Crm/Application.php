<?php

namespace Crm;

use \workerbeeeu\autodb\Autodb;
use \workerbeeeu\autodb\Blueprints\Application as ApplicationInterface;

/**
 * Custom Hooks für die Anpassunge von Autodb während der Laufzeit.
 * @author Maik Lampe <ml@easy-media.de>
 */
class Application implements ApplicationInterface {

    public function beforeStartup(Autodb $Autodb) {
        // Wir arbeiten mit utf-8
        $Autodb->getPdo()->exec("set names utf8");
        // wir ersetzen den autodb nutzer durch einen eigenen
        $newUser = new Model\User($Autodb->getPdo());
        $newUser->current();
        $Autodb->setUser($newUser);
    }

    public function afterRun(Autodb $Autodb) {

    }

    public function afterStartup(Autodb $Autodb) {

    }

    public function beforeRedirect($arrParams, $strTargetUrl, Autodb $Autodb) {

    }

    public function beforeRun($strCalledController, $strCalledAction, Autodb $Autodb) {

    }

}
