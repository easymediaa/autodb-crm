<?php
$startTime = microtime(true);

include_once './application/init.php';

$Autodb = new \workerbeeeu\autodb\Autodb(new Crm\Application());

try {
    $Autodb->run();
} catch (\Exception $exc) {
    $objOrig = $exc;
    include_once 'application/' . $Autodb->getConfig()->getVar("routing", "application") . '/View/fatal.php';
    $exc = $objOrig;
    do {
        \workerbeeeu\autodb\Entity\Log::log(get_class($exc) . " in " . $exc->getFile() . ":" . $exc->getLine(), $exc->getMessage(), $exc->getCode());
    } while ($exc = $exc->getPrevious());
}

$stopTime = microtime(true)
?>
<fieldset class="debug col-xs-12">
    <legend>RAM Usage & Parsetime</legend>
    <a href="?">Dashboard</a>
    <a href="?controller=session&action=logout">Logout</a>
    <pre style="font-family: monospace"><?php
        var_dump(array(
            "start" => $startTime,
            "finish" => $stopTime,
            "runtime" => number_format($stopTime - $startTime, 4, ",", ".") . " sec",
            'memory_get_uage' => array(
                'normal' => number_format(memory_get_usage() / 1024, 2, ",", ".") . " kb",
                'real_usage' => number_format(memory_get_usage(true) / 1024, 2, ",", ".") . " kb"
            ),
            'memory_get_peak_usage' => array(
                'normal' => number_format(memory_get_peak_usage() / 1024, 2, ",", ".") . " kb",
                'real_usage' => number_format(memory_get_peak_usage(true) / 1024, 2, ",", ".") . " kb"
            )
        ));
        var_dump(array(
            "user" => $Autodb->getUser(),
            "pdo" => $Autodb->getPdo()
        ));
        ?>
    </pre>
</fieldset>
